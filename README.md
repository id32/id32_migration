# ID32_migration

Path to BLISS tasks for ID32

*Updated the 30/11/2023, L.Claustre*

## Bliss upgrade strategy for ID32

- lid321 (2013): Upgraded to debian8 **UPSTREAM SIDE**
  - PLCvacuumValve ID32
  - VacGaugeServer ID32gauges ID32ip
  - Multiplexer tango for 4 OPIOM (mono, RIXS, XMCD, DIFF)
  - GpibController_PCI
  - ser2net ??
  - OPIOM Mux mono 1
  - MUSST zap mono

- lid322 (2013): redhate4          **RIXS BRANCH**
  - ct2ds lid322_00
  - hexapode rfo_01
  - PLCvacuumValve ID32_RIXS
  - Serialds 322_232_04 322_232_05
  - VacGaugeServer ID32gauges_RIXS ID32ip_RIXS
  - Wago wcid32n
  - ser2net ???
  - OPIOM Mux RIXS 4

- lid323 (2013): redhate4           **XMCD BRANCH**
  - ct2ds lid323_00
  - hexapode hexa_hrm_01 hexa_vrm_01 hexa_dm_01 hexa_def_01
  - Mcce mcce323_0 mcce323_1
  - Serialds 323_232_01 02 03 06 16 15 16
  - VacGaugeServer ID32gauges_XMCD ID32ip_XMCD
  - ser2net ??
  - OPIOM Mux XMCD 2 DIFF 3

- lid324 (2013): ubuntu 20.04 (since june 23)           **DIFFRACTO BRANCH**
  - ct2ds lid324_00
  - Wago wcid32m
  - ser2net ??

- lid323bis (2011): Upgraded to debian8
  - P201 bliss server + tango
  - Wago tango c++ wcid32a/b/c/d/e/f/g/h/i/j/k/l/cryo1
  - MKS_MicrovisionIP_RGA tango c++ diff id32 microvision rga04 spf
  - RGADiag tango c++ diff id32 microvision rga04 spf
  - PLCvacuumValve ID32_XMCS

- lid32bas1: Upgraded to ubuntu 20.04
  - LimaCCDs dg2 dg3 dg4 diagon (acA1300-30mg)
  - LimaBpmWebServer id32bas1

- lid32bas2: Upgraded to ubuntu 20.04
  - LimaCCDs dg5a dg5b dg6a dg7b bva bvb (acA1300-30mg)
  - LimaBpmWebServer id32bas2

- lid32det1: Upgraded to debian8
  - LimaCCDS andor_1 andor_2

- lid32det2: ubuntu 20.04
  - LimaCCDs dhyana95v2

- lid32det3 ubuntu 20.04
  - ??

- lid32ctrl1: debian8            **ID32 MAIN WORKSTATION 2019**
  - tango_host
  - beacon_host
  - SPEC optics_exph (server)

- lid32xmcd1: ubuntu 20.04 (October 23)    **XMCD WORKSTATION 2019**
  - SPEC optics_xmcd xmcd xmcd_user
  - LimaCCDs microscope (basler acA1300-30gm) 

- lid32rixs1: ubuntu 20.04 (October 23)            **RIXS WORKSTATION 2020**
  - SPEC rixs rixs_user


- lid32diff1: ubuntu 20.04 (October 20.04)            **DIFF WORKSTATION 2021**
  - SPEC fourc_diff
  - LimaCCDs dhyana95

- Use lid32diff1 (and lid324) to test/develop/configure all ID32 devices
  - CONFIGURE/TEST
    - Icepap
    - Temperature: **Wago**
    - Temperature:   **Lakeshore 340 / 336 / 332**
    - Power Supply **: NHQ204**
    - ID32 slits
    - ESRF Slits (4 blades)
    - XBPM standard
    - KB focus
    - Ice shutter
    - Opiom
    - MUSST
    - MCCE (taco server -> serial on lid323 ttyR30 and ttyR31, 4 mcce) only use for from xmcd exp.
    - LIMA Andor
    - Scan with P201 / MUSST / ANDOR
    - Hexapode
    - Undulators
    - BCDU8
    - Keithley 428 / 427
    - Gauss Meter: **Lakeshore 220**
    - FEMTO (to be merged in bliss repo)
  - DEVELOP
    - MAGNET
    - Power Supply: Kepco BopG 10-100
    - XBPM **diagon**
    

- Use lid32diff1 to rewrite/configure/test Important ID32 sequences
  - Double Mirror
  - Monochromator PGM
  - Deflecting Mirror
  - Gaz Cell
  - Refocusing Optics
  - RIXS Sample chamber
  - Fourc Spectrometer
  - HRM / VRM
  - ZAP MONO
  - ZAP MAGNET
  - **Monochromator exclusivity between End Station**

## Retro Planning

- January 2024: Restart USM with Bliss control
- Nov-Dec 28-8 2023: beamtime commissioning with BLISS for the Spectrometer
- September 2023: Beamline for ID32 staff for Bliss or SPEC tests (NO BCU development anymore) for all branches
- July-August 26-1st 29-11 Sept 2023 : beamtime for commissioning: beamline alignement, mono contscan on DIFF branch only
- May 2023 shutdown: test Bliss mono  energy contscan
- March-May 2023: monochromator configuration for exafs energy scan, beamline configuration (motors,electronics ctrl...)
- March 2023 shutdown: zap macros port to P201 bliss device server

## Machine upgrade / OS upgrade / BLISS installation

- lid321
  - ubuntu 20.04
  - BLISS installation
  - Serial Line / Gpib

- lid322
  - ubuntu 20.04
  - BLISS installation
  - ser2net
  - P201

- lid323
  - ubuntu 20.04
  - Bliss installation
  - ser2net
  - P201

- lid324
  - ubuntu 20.04
  - BLISS installation
  - ser2net
  - P201
  
- wid322
  - Windows ??
  - BLISS installation
  - XIA ??

- Unknown Windows pc
  - pingable: wid32pc1 / wid32pc2 / wid32pc5 / wid32pc6  / wid32pc10 / wid32pc12
  - not pingable:  wid32pc3 / wid32pc4 / wid32pc7 / wid32pc8 / wid32pc9 / wid32pc11 / wid32pc13

- lid32det1
  - debian8 -> ubuntu 20.04
  - USB
  -  LIMA Andor

- lid32det2 nothing to do

- lid32det3 ??

- lid32ctrl1
  - debian8 -> ubuntu 20.04
  - BLISS installation
  - Beacon HOST
  - Tango HOST
  - icepapcms

- lrixs1
  - debian8 -> ubuntu 20.04
  - BLISS installation
  - Jvacuum
  - PYMCA

- lxmcd1
  - debian8 -> ubuntu 20.04
  - BLISS installation
  - BPM Web appli
  - Jvacuum
  - PYMCA

- lid32diff1
  - debian8 -> ubuntu 20.04
  - BLISS installation
  - Jvacuum
  - PYMCA

## Devices

#### Vacuum

- Tango config
- Devices servers startup
- Jvacuum on lid32ctrl / lxmcd1 / lrixs1 / ldiff1

#### Undulators

- 3 undulators
- 3 motors / undulator: gap  / phase / offset
- Check if phase and offset exist in BLISS?
- Need to change speed and acceleration dynamically?
- Only 2 undulators at restart
- Add Phasing Unit
- !!! New control  !!!
- Ask ACU if undulator simulator exists

#### Temperatures

- Wago
  - PS (x9)
  - Diagon (x1)
  - DM (x8)
  - Bremsstrahlung stop (x1)
  - SS (x4)
  - Bremsstrahlung collimator (x2)
  - ENT (x2)
  - PGM (x10)
- Lakeshore 340
  - PGM (x4) / Magnet (x8)
- Lakeshore 332
  - PGM (x2) / Rixs Chamber (x2)
- Lakeshore 336
  - Magnet (x4)

#### Gauss Meter

- Lakeshore 220

#### Power Supply

- NHQ204
- BopG 10-100

#### Flow Meter

- Bronkhorst
  - Rixs Chamber / Magnet
- Sierra (DO NOT Upgrade !!!)
  - Magnet

#### Slits (4 blades)

- 4 blades
- PS / SS / MONO / B1A / B1B / B2B / B3B

#### Slits (ID32)

- Gap / x / z / rotx
- ENT / GS / EXA /EXB

#### XBPM

- Standard (!! Check diode measurement !!)
  - DG2 / DG3 / DG4 / DG5A / DG5B
  - In/Out: Wago
  - Beamviewer: Basler
  - Femto
- Specific
  - Diagon (DG1)
    - In/Out: Icepap
    - Beamviewer: Basler

#### Basler

- Tango config
- Install, test and ask for improvement for  Beam Viewer WEB application

#### Femto

DG2 / DG3 / DG4 / DEF / GazCellA / DG5A / RIXS Chamber / GazCellB / DG5B / VRM / Femto

- Wago + SPEC macros (femto.mac) TODO

#### Hexapode

- Double Mirror (DM)
- Deflecting Mirror (DEF)
- Refocusing Optics (RFO)
- Horizontal  Refocusing Mirror (HRM)
- Vertical Refocusing Mirror (VRM)

#### KB focus

- RFO / HRM / VRM

#### Keithley K427 (MANUAL)

- Magnet

#### Keithley K428

- Gaz cell A / RFO / RIXS Chamber / Gaz Cell B / I0 EC3 / Holo (x2)

#### Shutter

- Ice Shutter

#### EU Card

- Opiom
- MUSST
- BCDU8

## ID32 Equipment

#### Double Mirror (DM)

- Hexapode
- Rotz toroid (E753)

#### Monochromator PGM

- Icepap (x6)
- Monochromator parameters
- Monochromator continuous  scan
- Cryocooler (wago) ???

#### Deflecting Mirror (DEF)

- Hexapode
- Femto

#### Gaz Cell A

- Drain current : Keithley 428
- Photodiode: Femto
- Icepap (x2)

#### Refocusing Optics (RFO)

- Hexapode
- Bender (KB focus?)
- Drain Current : Kethley K428

#### RIXS Sample Chamber

- Fourc
- Detector
  - Kethley 428 (x2)
  - Femto
  - Power Supply: NHQ204
  - Temperature: Wago, Bronkhorst, Lakeshore332

#### RIXS Spectrometer

- Arm: Icepap (x5) Linked AXIS
- CLM: Icepap (x4)
- Gratings: Icepap (x8)
- Detector stage: Icepap (x3) Linked Axis
- Polarimeter: Icepap (x6)
- Detector:
  - Lima Andor iKonL (x2)
- Entrance / Exit Arms (Macros Motors)
- Air Pads: Wago Interlock (x14)

#### Gaz Cell B

- Drain current : Keithley 428
- Photodiode: Femto
- Icepap (x2)

#### Horizontal Refocusing Mirror (HRM)

- Hexapode
- Bender (KBfocus)
- Rotz toroid (E753)

#### Vertical Refocusing Mirror (VRM)

- Hexapode
- Bender (KBfocus)
- Rotz toroid (E753)
- Femto

#### I0

- Icepap (x2)
- Electrometer: MCCE2

#### Microscope

- Basler (Use GUI microscope Application)

#### High Field Magnet

- Icepap (x5)
- Magnet Power Supply ( HFM9tps.mac)
- Lakeshore 340
- Lakeshore 336
- Gaz flow
  - Bronkhorst
  - Sierra (im180.mac) (NOTHING TO DO)
- Needle Valve (nvalve.mac)
- Electrometer MCCE2
- Keithley K427 (NOTHING TO DO)

#### Pinhole

- Icepap (x2)
- Femto

#### I0 EC3

- Icepap (x1)
- Keithley K428

#### Diffractometer Scienta

- Icepap (x10)
- fourc

#### Holography Setup

- Icepap (x8)
- Keithley K428
- Power supply: BopG10-100 (Instrument Pool: Check changes)
- Lakeshore 220

## Sequences

To be filled by K. Kummer and F. yakhou

- energy continuous scans with undulator followers
- monochromator BLISS controller
